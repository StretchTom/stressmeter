package com.example.tmutabazi.stressmeter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;


public class MainActivity extends ActionBarActivity {

    int page = 1;
    Button morePhotos,graph;
    GridView gridview;
    SampleAlarmReceiver alarm = new SampleAlarmReceiver();
    Vibrator vib;
    MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        mp = MediaPlayer.create(getApplicationContext(), notification);
        if (getIntent().getBooleanExtra("getOut", false))
        {
            vib.cancel();
            mp.stop();
            finish();
        } else {
            alarm.setAlarm(this);
            morePhotos = (Button) findViewById(R.id.button);
            graph = (Button) findViewById(R.id.graph);
            gridview = (GridView) findViewById(R.id.gridview);
            gridview.setAdapter(new ImageAdapter(this, 1));

            vib.vibrate(40000);
            mp.start();
            morePhotos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    page = page + 1;
                    if (page == 4) {
                        page = 1;
                    }
                    gridview.setAdapter(new ImageAdapter(v.getContext(), page));
                }
            });


            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    //   Toast.makeText(MainActivity.this, "" + position,
                    //        Toast.LENGTH_SHORT).show();

                    vib.cancel();
                    mp.stop();
                    Intent ip = new Intent(MainActivity.this, imageActivity.class);
                    ip.putExtra("imageNumber", position);
                    ip.putExtra("pageNumber", page);
                    v.getContext().startActivity(ip);
                }
            });

            graph.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    vib.cancel();
                    mp.stop();
                    Intent ip = new Intent(MainActivity.this, Graph.class);
                    v.getContext().startActivity(ip);
                }
            });
        }


    }



}
