package com.example.tmutabazi.stressmeter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.sql.DataSource;


public class imageActivity extends ActionBarActivity {

    Button back,submit;
    ImageView imageView;
    ArrayList<Integer> list;
    private File mFeatureFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);


        back = (Button) findViewById(R.id.button2);
        submit = (Button) findViewById(R.id.button3);
        imageView = (ImageView) findViewById(R.id.imageView);
        list = new ArrayList<Integer>();
        list.add(6);
        list.add(8);
        list.add(14);
        list.add(16);
        list.add(5);
        list.add(7);
        list.add(13);
        list.add(15);
        list.add(2);
        list.add(4);
        list.add(10);
        list.add(12);
        list.add(1);
        list.add(3);
        list.add(9);
        list.add(11);



        Intent intent = getIntent();
        int pageNumber = intent.getIntExtra("pageNumber",2);
        final int imageNumber =  intent.getIntExtra("imageNumber", 1);
        int id = PSM.returnid(pageNumber,imageNumber);
        imageView.setImageResource(id);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                writeInFile(list.get(imageNumber));
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("getOut", true);
                startActivity(intent);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image, menu);
        return true;
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void writeInFile(int score)
    {
        try {
            mFeatureFile = new File(getExternalFilesDir(null), "acc_raw.csv");
            Log.d("path", mFeatureFile.getAbsolutePath());
            DataSource source;
            FileOutputStream fOut;
            if(!mFeatureFile.exists())
            {
                mFeatureFile.createNewFile();
                fOut = new FileOutputStream(mFeatureFile);
            }
            else
            fOut = new FileOutputStream (mFeatureFile.getAbsolutePath().toString(), true);
            OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTimeStamp = date.format(new Date());
            myOutWriter.append(currentTimeStamp+","+score+"\n");
            myOutWriter.close();
            fOut.close();
            Toast.makeText(getBaseContext(),"Writing in thw file ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

}
