package com.example.tmutabazi.stressmeter;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Graph extends ActionBarActivity {

    List data = new ArrayList();
    ArrayList<String> x = new ArrayList();
    ArrayList<Integer> y = new ArrayList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_graph);


        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("/storage/emulated/0/Android/data/com.example.tmutabazi.stressmeter/files/acc_raw.csv");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader buffRead = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            int z =0;
            while ((csvLine = buffRead.readLine()) != null) {
                String[] row = csvLine.split(",");
                //data.add(row);
                x.add(row[0]);
                y.add(Integer.parseInt(row[1]));

            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                inputStream.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        LineChart chart = new LineChart(this);
        setContentView(chart);

        ArrayList<Entry> entries = new ArrayList<>();


        for(int i=0;i<y.size();i++)
        {
            entries.add(new Entry(y.get(i),i));
        }
        LineDataSet dataSet = new LineDataSet(entries," # Stress score");

        LineData data = new LineData(x,dataSet);

        chart.setData(data);

    }



}
